package rest

import (
	"power-factors-assignment/internal/ptservice"
	"power-factors-assignment/internal/ptservice/domain/ptask"
)

func ErrorToJSON(err string) Error {
	return Error{
		Status: "error",
		Desc:   err,
	}
}

func PeriodicTaskToJSON(task ptask.Task) PeriodicTask {
	return task.Invocation.Format(ptservice.DateTimeFormat)
}
