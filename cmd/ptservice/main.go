package main

import (
	"fmt"
	"github.com/pkg/errors"
	"log"
	"net"
	"os"
	"os/signal"
	"power-factors-assignment/internal/ptservice"
	"power-factors-assignment/internal/ptservice/rest"
	"syscall"
)

func main() {
	cfg := parseConfig()

	app := ptservice.New()

	server := rest.NewServer(app)

	addr := fmt.Sprintf("%s:%d", cfg.Host, cfg.Port)

	// create tcp listener for our server to listen on
	log.Printf("Starting server on %s ...\n", addr)
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	// close the listener on interrupt
	go func() {
		<-interrupt
		_ = ln.Close()
	}()

	// errChan captures the error returned by server.Run. net.ErrClosed is ignored
	errChan := make(chan error, 1)
	go func() {
		log.Println("Server started.")

		err = server.Run(ln)
		if errors.Is(err, net.ErrClosed) {
			err = nil
		}
		errChan <- err
	}()

	// wait for the listener to close
	err = <-errChan
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Server stopped.")
}
