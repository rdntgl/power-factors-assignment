package main

import (
	"github.com/namsral/flag"
)

type Config struct {
	Host string
	Port int
}

func parseConfig() Config {
	cfg := Config{}

	flag.StringVar(&cfg.Host, "host", "", "Host the server should listen on")
	flag.IntVar(&cfg.Port, "port", 8080, "Port the server should listen on")

	flag.Parse()

	return cfg
}
