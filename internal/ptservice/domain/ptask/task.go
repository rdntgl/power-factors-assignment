// Package ptask includes definitions for periodic tasks & related
package ptask

import "time"

// Task defines a periodic task of sorts
type Task struct {
	// Period is the period of the task
	Period Period
	// Invocation is the time after the period start the task should be invoked at
	Invocation time.Time
	// Location is the location/timezone that this task was created for.
	// Useful in cases such as query periods of 1d, 1mo, 1y.
	Location *time.Location
}
