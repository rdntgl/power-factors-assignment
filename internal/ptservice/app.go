package ptservice

import (
	"github.com/pkg/errors"
	"github.com/teambition/rrule-go"
	"power-factors-assignment/internal/ptservice/domain/ptask"
	"time"
)

const DateTimeFormat = rrule.DateTimeFormat

var (
	ErrInvalidPeriod = errors.New("invalid period")
	ErrInvalidRange  = errors.New("invalid range")
)

type App struct {
}

func New() *App {
	return &App{}
}

// GetPeriodTasksInRange returns a list of periodic tasks that are within the
// given range, based on the time period specified.
// If the provided period is not defined, ErrInvalidPeriod is returned.
// If the provided time range is invalid, ErrInvalidRange is returned.
func (app *App) GetPeriodTasksInRange(period ptask.Period, loc *time.Location, t1 time.Time, t2 time.Time) ([]ptask.Task, error) {
	if t1.After(t2) {
		return nil, ErrInvalidRange
	}

	// calculate rule frequency for this period.
	//  try to resolve the recurring intervals (at least for
	//  daily/monthly/yearly) in the location/timezone provided.
	var freq rrule.Frequency
	switch period {
	case ptask.PeriodHourly:
		freq = rrule.HOURLY
		t1 = t1.Round(time.Hour).In(loc)
		t2 = t2.In(loc)
	case ptask.PeriodDaily:
		freq = rrule.DAILY
		t1 = time.Date(t1.Year(), t1.Month(), t1.Day()+1, 0, 0, 0, 0, loc)
		t2 = t2.In(loc)
	case ptask.PeriodMonthly:
		freq = rrule.MONTHLY
		t1 = time.Date(t1.Year(), t1.Month()+1, 1, 0, 0, 0, 0, loc)
		t2 = t2.In(loc)
	case ptask.PeriodYearly:
		freq = rrule.YEARLY
		t1 = time.Date(t1.Year()+1, 1, 1, 0, 0, 0, 0, loc)
		t2 = t2.In(loc)
	default:
		return nil, ErrInvalidPeriod
	}

	r, err := rrule.NewRRule(rrule.ROption{
		Freq:    freq,
		Dtstart: t1,
		Until:   t2,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create rrule")
	}

	times := r.All()

	tasks := make([]ptask.Task, len(times))
	for i, t := range times {
		tasks[i] = ptask.Task{
			Period:     period,
			Invocation: t.In(time.UTC),
			Location:   loc,
		}
	}

	return tasks, nil
}
