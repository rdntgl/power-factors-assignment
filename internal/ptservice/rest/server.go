package rest

import (
	"net"
	"net/http"
	"power-factors-assignment/internal/ptservice"
	"power-factors-assignment/internal/ptservice/domain/ptask"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

// generates the REST server models from openapi spec
//go:generate oapi-codegen  --config oapi-codegen.yaml openapi.json

// Server is the HTTP REST server of ptservice
type Server struct {
	r   *gin.Engine
	app *ptservice.App
}

func NewServer(app *ptservice.App) *Server {
	gin.SetMode(gin.ReleaseMode)

	s := &Server{
		r:   gin.New(),
		app: app,
	}
	_ = s.r.SetTrustedProxies(nil)

	s.r.GET("/ptlist", s.GetPeriodicTasksInRange)
	s.r.NoRoute(NoRouteHandler)

	return s
}

var ErrListenFailed = errors.New("listen failed")

func (s *Server) Run(ln net.Listener) error {
	return s.r.RunListener(ln)
}

func (s *Server) GetPeriodicTasksInRange(c *gin.Context) {
	var q GetPeriodicTasksInRangeParams
	err := c.ShouldBindQuery(&q)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorToJSON("invalid request"))
		return
	}

	loc, err := time.LoadLocation(q.Tz)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorToJSON("invalid tz"))
		return
	}

	t1, err := time.Parse(ptservice.DateTimeFormat, q.T1)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorToJSON("invalid t1"))
		return
	}

	t2, err := time.Parse(ptservice.DateTimeFormat, q.T2)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorToJSON("invalid t2"))
		return
	}

	tasks, err := s.app.GetPeriodTasksInRange(
		ptask.Period(q.Period),
		loc, t1, t2,
	)
	switch {
	case errors.Is(err, ptservice.ErrInvalidPeriod):
		c.JSON(http.StatusBadRequest, ErrorToJSON("Unsupported period"))
		return
	case errors.Is(err, ptservice.ErrInvalidRange):
		c.JSON(http.StatusBadRequest, ErrorToJSON("invalid time range"))
		return
	}

	resp := make(GetPeriodicTasksInRangeResponse, len(tasks))
	for i, t := range tasks {
		resp[i] = PeriodicTaskToJSON(t)
	}

	c.JSON(http.StatusOK, resp)
}
