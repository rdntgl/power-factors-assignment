package ptask

type Period string

const (
	PeriodHourly  Period = "1h"
	PeriodDaily   Period = "1d"
	PeriodMonthly Period = "1mo"
	PeriodYearly  Period = "1y"
)
