# ptservice

ptservice is Go a service that implements the requirements of the assignment.
It consists of an internal application (encapsulating application logic & the
domain entities) and an HTTP REST API server that exposes the app to the
outside world.

Requirements:
- Go 1.20
- Bash
- Docker (for the container image)
- Git Bash (if you are on Windows)

Tested on Windows 11 (native) and GitHub codespaces (Ubuntu 20)

### Bootstrap

```bash
# generate the API models
go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@latest
go generate ./...
```

### Running locally

```bash
# windows disclaimer: you're better off building the server and running it
#  in docker because of microsoft defender ¯\_(ツ)_/¯
go run ./cmd/ptservice/...
```

### Build the server

```bash
GOOS=linux GOARCH=amd64 go build -o "./cmd/ptservice/build/server" ./cmd/ptservice/...
```

### Build the container image

```bash
docker build -t ptservice:latest ./cmd/ptservice
```

### Run the container

```bash
docker run -itp 8080:8080 ptservice:latest
```
