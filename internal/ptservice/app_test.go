package ptservice_test

import (
	"fmt"
	"github.com/samber/lo"
	"gotest.tools/v3/assert"
	"power-factors-assignment/internal/ptservice"
	"power-factors-assignment/internal/ptservice/domain/ptask"
	"testing"
	"time"
)

func TestApplication_GetPeriodTasksInRange(t *testing.T) {
	app := ptservice.App{}

	type testCase struct {
		period ptask.Period
		start  string
		end    string
		want   []string
		err    error
	}

	tcs := []testCase{
		{
			period: ptask.PeriodHourly,
			start:  "20210714T204603Z",
			end:    "20210715T123456Z",
			want: []string{
				"20210714T210000Z",
				"20210714T220000Z",
				"20210714T230000Z",
				"20210715T000000Z",
				"20210715T010000Z",
				"20210715T020000Z",
				"20210715T030000Z",
				"20210715T040000Z",
				"20210715T050000Z",
				"20210715T060000Z",
				"20210715T070000Z",
				"20210715T080000Z",
				"20210715T090000Z",
				"20210715T100000Z",
				"20210715T110000Z",
				"20210715T120000Z",
			},
			err: nil,
		},
		{
			period: ptask.PeriodDaily,
			start:  "20211010T204603Z",
			end:    "20211115T123456Z",
			want: []string{
				"20211010T210000Z",
				"20211011T210000Z",
				"20211012T210000Z",
				"20211013T210000Z",
				"20211014T210000Z",
				"20211015T210000Z",
				"20211016T210000Z",
				"20211017T210000Z",
				"20211018T210000Z",
				"20211019T210000Z",
				"20211020T210000Z",
				"20211021T210000Z",
				"20211022T210000Z",
				"20211023T210000Z",
				"20211024T210000Z",
				"20211025T210000Z",
				"20211026T210000Z",
				"20211027T210000Z",
				"20211028T210000Z",
				"20211029T210000Z",
				"20211030T210000Z",
				"20211031T220000Z",
				"20211101T220000Z",
				"20211102T220000Z",
				"20211103T220000Z",
				"20211104T220000Z",
				"20211105T220000Z",
				"20211106T220000Z",
				"20211107T220000Z",
				"20211108T220000Z",
				"20211109T220000Z",
				"20211110T220000Z",
				"20211111T220000Z",
				"20211112T220000Z",
				"20211113T220000Z",
				"20211114T220000Z",
			},
			err: nil,
		},
		{
			period: ptask.PeriodMonthly,
			start:  "20210214T204603Z",
			end:    "20211115T123456Z",
			want: []string{
				"20210228T220000Z",
				"20210331T210000Z",
				"20210430T210000Z",
				"20210531T210000Z",
				"20210630T210000Z",
				"20210731T210000Z",
				"20210831T210000Z",
				"20210930T210000Z",
				"20211031T220000Z",
			},
			err: nil,
		},
		{
			period: ptask.PeriodYearly,
			start:  "20180214T204603Z",
			end:    "20211115T123456Z",
			want: []string{
				"20181231T220000Z",
				"20191231T220000Z",
				"20201231T220000Z",
			},
			err: nil,
		},
	}

	for _, tc := range tcs {
		t.Run(fmt.Sprintf("%s-%s-%s", tc.period, tc.start, tc.end), func(t *testing.T) {
			t1, err := time.Parse(ptservice.DateTimeFormat, tc.start)
			assert.NilError(t, err)

			t2, err := time.Parse(ptservice.DateTimeFormat, tc.end)
			assert.NilError(t, err)

			loc, err := time.LoadLocation("Europe/Athens")
			assert.NilError(t, err)

			tasks, err := app.GetPeriodTasksInRange(tc.period, loc, t1.UTC(), t2.UTC())
			assert.NilError(t, err)

			for _, task := range tasks {
				assert.Equal(t, task.Period, tc.period)
				assert.Equal(t, task.Location, loc)
			}

			got := lo.Map(tasks, func(task ptask.Task, _ int) string {
				return task.Invocation.Format(ptservice.DateTimeFormat)
			})

			assert.DeepEqual(t, got, tc.want)
		})
	}
}
